//
//  SchoolListViewModel.swift
//  20190708-RussellStephenson-NYCSchools
//
//  Created by Russell Stephenson on 7/6/19.
//  Copyright © 2019 RussellApps. All rights reserved.
//

import Foundation

protocol SchoolListViewModelType: class {
    var schools: [School]? { get }
    
    func loadSchools(completion: @escaping () -> Void)
    func getSchool(index: Int) -> School?
    func sortSchools(sortOrder: Sorting, completion: @escaping () -> Void)
}

final class SchoolListViewModel: SchoolListViewModelType {
    
    // MARK: Public Properties
    
    var schools: [School]? {
        return schoolList
    }
    
    // MARK: Private Properties
    
    private var schoolList: [School]?
    private var downloadTask : URLSessionTask?
    
    // MARK: Initialization
    
    init() { }
    
    // MARK: Get Locations
    
    func loadSchools(completion: @escaping () -> Void) {
        guard let url = URL(string: Constants.API.schoolListUrl) else {
            return
        }
        
        DispatchQueue.main.async {
            if self.downloadTask?.progress.isCancellable ?? false {
                self.downloadTask?.cancel()
            }
            let task = URLSession.shared.dataTask(with: url) { [weak self] (data, response, error) in
                guard error == nil else {
                    self?.displayError(error: error!)
                    return
                }
                guard let data = data else {
                    return
                }
                let schoolList : [School]?
                let decoder = JSONDecoder();
                decoder.dateDecodingStrategy = .iso8601
                do {
                    schoolList = try decoder.decode(Array.self, from: data)
                } catch {
                    self?.displayError(error: error)
                    return
                }
                
                if let list = schoolList {
                    self?.schoolList = list
                }
                
                DispatchQueue.main.async {
                    completion()
                }
            }
            task.resume()
            self.downloadTask = task
        }
        //sortLocations(location: nil)
    }
    
    func getSchool(index: Int) -> School? {
        return schoolList?[optional: index]
    }
    
    func sortSchools(sortOrder: Sorting, completion: @escaping () -> Void) {
        guard let schools = schoolList else {
            return
        }
        
        switch sortOrder {
        case .alphabeticalByName(ascending: true):
            schoolList = schools.sorted() {
                return $0.school_name < $1.school_name
            }
            completion()
        case .alphabeticalByName(ascending: false):
            schoolList = schools.sorted() {
                return $0.school_name > $1.school_name
            }
            completion()
        case .alphabeticalByCity(ascending: true):
            schoolList = schools.sorted() {
                guard let city1 = $0.city,
                    let city2 = $1.city else {
                        return false
                }
                return city1 < city2
            }
            completion()
        case .alphabeticalByCity(ascending: false):
            schoolList = schools.sorted() {
                guard let city1 = $0.city,
                    let city2 = $1.city else {
                        return false
                }
                return city1 > city2
            }
            completion()
        case .numericallyByGraduationPercent(ascending: true):
            schoolList = schools.sorted(by: {
                guard let percent1 = $0.graduation_rate,
                    let percent1Double = Double(percent1),
                    let percent2 = $1.graduation_rate,
                    let percent2Double = Double(percent2) else {
                        return true
                }
                return percent1Double > percent2Double
                
            })
            completion()
        case .numericallyByGraduationPercent(ascending: false):
            schoolList = schools.sorted(by: {
                guard let percent1 = $0.graduation_rate,
                    let percent1Double = Double(percent1),
                    let percent2 = $1.graduation_rate,
                    let percent2Double = Double(percent2) else {
                        return true
                }
                return percent1Double < percent2Double
            })
            completion()
        case .numericallyByNumberOfStudents(ascending: true):
            schoolList = schools.sorted(by: {
                guard let number1 = $0.total_students,
                    let number1Int = Int(number1),
                    let number2 = $1.total_students,
                    let number2Int = Int(number2) else {
                        return false
                }
                return number1Int > number2Int
            })
            completion()
        case .numericallyByNumberOfStudents(ascending: false):
            schoolList = schools.sorted(by: {
                guard let number1 = $0.total_students,
                    let number1Int = Int(number1),
                    let number2 = $1.total_students,
                    let number2Int = Int(number2) else {
                        return false
                }
                return number1Int < number2Int
            })
            completion()
        }
    }
    
    func displayError(error: Error) {
        print("Error: \(error.localizedDescription)")
    }
}

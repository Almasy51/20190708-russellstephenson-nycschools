//
//  SchoolViewModel.swift
//  20190708-RussellStephenson-NYCSchools
//
//  Created by Russell Stephenson on 7/6/19.
//  Copyright © 2019 RussellApps. All rights reserved.
//

import Foundation

protocol SchoolViewModelType: class {
    var school: School? { get }
    
    func loadSatScores(completion: @escaping () -> Void)
    func getSatScore() -> SatScore?
}

final class SchoolViewModel: SchoolViewModelType {
    
    // MARK: Public Properties
    
    var school: School? {
        return currentSchool
    }
    
    // MARK: Private Properties
    
    private var currentSchool: School?
    private var satScores: [SatScore]?
    private var downloadTask : URLSessionTask?
    
    // MARK: Initialization
    
    init(school: School) {
        self.currentSchool = school
    }
    
    // MARK: Get Locations
    
    func loadSatScores(completion: @escaping () -> Void) {
        if let wrapper = NSCacheHelper.shared.get(for: NSString(string: Constants.Storage.SatScoreKey)) as? Wrapper,
            let satScores = wrapper.storedValue as? [SatScore] {
            self.satScores = satScores
            completion()
        }
        
        guard let url = URL(string: Constants.API.satScoreUrl) else {
            return
        }
        
        DispatchQueue.main.async {
            if self.downloadTask?.progress.isCancellable ?? false {
                self.downloadTask?.cancel()
            }
            let task = URLSession.shared.dataTask(with: url) { [weak self] (data, response, error) in
                guard error == nil else {
                    self?.displayError(error: error!)
                    return
                }
                guard let data = data else {
                    return
                }
                let scoreList : [SatScore]?
                let decoder = JSONDecoder();
                decoder.dateDecodingStrategy = .iso8601
                do {
                    scoreList = try decoder.decode(Array.self, from: data)
                } catch {
                    self?.displayError(error: error)
                    return
                }
                
                if let list = scoreList {
                    NSCacheHelper.shared.set(object: Wrapper(storedValue: list), key: Constants.Storage.SatScoreKey as NSString)
                    self?.satScores = list
                }
                
                DispatchQueue.main.async {
                    completion()
                }
            }
            task.resume()
            self.downloadTask = task
        }
    }
    
    func getSatScore() -> SatScore? {
        guard let scores = satScores,
        let school = self.currentSchool else {
            return nil
        }
        
        for score in scores {
            if score.dbn == school.dbn {
                return score
            }
        }
        return nil
    }
    
    func displayError(error: Error) {
        print("Error: \(error.localizedDescription)")
    }
}

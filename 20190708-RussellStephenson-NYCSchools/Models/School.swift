//
//  School.swift
//  20190708-RussellStephenson-NYCSchools
//
//  Created by Russell Stephenson on 7/6/19.
//  Copyright © 2019 RussellApps. All rights reserved.
//

import Foundation

struct School: Codable {
    let dbn: String
    let school_name: String
    let overview_paragraph: String?
    let phone_number: String?
    let fax_number: String?
    let school_email: String?
    let total_students: String?
    let extracurricular_activities: String?
    let school_sports: String?
    let primary_address_line_1: String?
    let city: String?
    let zip: String?
    let state_code: String?
    let attendance_rate: String?
    let graduation_rate: String?
}

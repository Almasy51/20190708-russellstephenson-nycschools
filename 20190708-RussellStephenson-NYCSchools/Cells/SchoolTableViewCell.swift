//
//  SchoolTableViewCell.swift
//  20190708-RussellStephenson-NYCSchools
//
//  Created by Russell Stephenson on 7/7/19.
//  Copyright © 2019 RussellApps. All rights reserved.
//

import UIKit

class SchoolTableViewCell: UITableViewCell {
    @IBOutlet weak var schoolName: UILabel!
    
    func setCell(school: School) {
        schoolName.text = school.school_name
    }
}

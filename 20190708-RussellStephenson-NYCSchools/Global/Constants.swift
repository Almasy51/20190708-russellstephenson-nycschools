//
//  Constants.swift
//  20190708-RussellStephenson-NYCSchools
//
//  Created by Russell Stephenson on 7/6/19.
//  Copyright © 2019 RussellApps. All rights reserved.
//

import Foundation

struct Constants {
    struct API {
        static let schoolListUrl = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
        static let satScoreUrl = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
    }
    
    struct Storage {
        static let SatScoreKey = "SatScoreKey"
    }
    
    struct CellIdentifiers {
        static let schoolCell = "SchoolCell"
    }
    
    struct SegueIdentifiers {
        static let schoolDetailSegue = "SchoolDetailSegue"
    }
}

enum Sorting {
    case alphabeticalByName(ascending: Bool)
    case alphabeticalByCity(ascending: Bool)
    case numericallyByGraduationPercent(ascending: Bool)
    case numericallyByNumberOfStudents(ascending: Bool)
}

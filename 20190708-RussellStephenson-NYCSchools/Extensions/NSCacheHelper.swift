//
//  NSCacheHelper.swift
//  20190708-RussellStephenson-NYCSchools
//
//  Created by Russell Stephenson on 7/6/19.
//  Copyright © 2019 RussellApps. All rights reserved.
//

import Foundation

final class NSCacheHelper {
    static let shared = NSCacheHelper()
    let cache = NSCache<NSString, AnyObject>()
    
    private init() {
        
    }
    
    func get(for key: NSString) -> AnyObject? {
        return cache.object(forKey: key)
    }
    
    func set(object: AnyObject, key: NSString) {
        return cache.setObject(object, forKey: key)
    }
}

class Wrapper {
    var storedValue: Any
    
    init(storedValue: Any) {
        self.storedValue = storedValue
    }
}

#  README.md

This project does not contain any additional third party frameworks.

The only issue that I have encountered is the School list doesn't properly sort using the Graduation Percentage. The only possible reason that I can think of is that some schools are missing graduation percentages and it's messing with the sorting in some way. Given more time I'm sure I could find a proper solution to the issue.

My general direction in creating the project was firstly looking at the data after understanding the goal of the project.
Next I created data models and the view models to make the API requests.
Then I created the list view and bound the view model and data to it, making sure the data displayed correctly.
Afterwards I created the second view and likewise bound the view model and data to it.
Once the hard requirements were finished I started making it look prettier, mostly the detail page.
Then I added sorting as a "nice to have" feature.
Lastly I created the tests for the API requests. Given more time I would have considered adding a UI test for the sorting selections.


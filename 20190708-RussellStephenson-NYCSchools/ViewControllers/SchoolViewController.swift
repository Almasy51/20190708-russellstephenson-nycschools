//
//  SchoolViewController.swift
//  20190708-RussellStephenson-NYCSchools
//
//  Created by Russell Stephenson on 7/6/19.
//  Copyright © 2019 RussellApps. All rights reserved.
//

import UIKit

class SchoolViewController: UIViewController {
    
    var viewModel: SchoolViewModel?
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var overviewLabel: UILabel!
    @IBOutlet weak var totalStudentsLabel: UILabel!
    @IBOutlet weak var graduationLabel: UILabel!
    @IBOutlet weak var attendanceLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var faxLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var stateLabel: UILabel!
    @IBOutlet weak var zipLabel: UILabel!
    @IBOutlet weak var activitiesLabel: UILabel!
    @IBOutlet weak var sportsLabel: UILabel!
    @IBOutlet weak var satNumberLabel: UILabel!
    @IBOutlet weak var satReadingLabel: UILabel!
    @IBOutlet weak var satMathLabel: UILabel!
    @IBOutlet weak var satWritingLabel: UILabel!
    @IBOutlet weak var satScoreLabel: UILabel!
    
    /// MARK: View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
    }
    
    func setup() {
        guard let model = viewModel else {
            return
        }
        
        model.loadSatScores { [weak self] in
            guard let school = model.school else {
                return
            }
            
            self?.nameLabel.text = school.school_name
            self?.overviewLabel.text = school.overview_paragraph
            self?.totalStudentsLabel.text = "Number of Students: \(school.total_students ?? "N/A")"
            self?.graduationLabel.text = "Graduation Rate: \(school.graduation_rate ?? "N/A")"
            self?.attendanceLabel.text = "Attendance Rate: \(school.attendance_rate ?? "N/A")"
            self?.phoneLabel.text = "Phone: \(school.phone_number ?? "N/A")"
            self?.faxLabel.text = "Fax: \(school.fax_number ?? "N/A")"
            self?.emailLabel.text = "Email: \(school.school_email ?? "N/A")"
            self?.addressLabel.text = school.primary_address_line_1
            self?.cityLabel.text = school.city
            self?.stateLabel.text = school.state_code
            self?.zipLabel.text = school.zip
            self?.activitiesLabel.text = school.extracurricular_activities
            self?.sportsLabel.text = school.school_sports
            
            if let satScores = model.getSatScore() {
                if satScores.num_of_sat_test_takers.isEmpty &&
                    satScores.sat_critical_reading_avg_score.isEmpty &&
                    satScores.sat_math_avg_score.isEmpty &&
                    satScores.sat_writing_avg_score.isEmpty {
                    self?.satScoreLabel.text = ""
                }
                
                if !satScores.num_of_sat_test_takers.isEmpty {
                    self?.satNumberLabel.text = "Number Testing: \(satScores.num_of_sat_test_takers)"
                }
                if !satScores.sat_critical_reading_avg_score.isEmpty {
                    self?.satReadingLabel.text = "Critical Reading: \(satScores.sat_critical_reading_avg_score)"
                }
                if !satScores.sat_math_avg_score.isEmpty {
                    self?.satMathLabel.text = "Math: \(satScores.sat_math_avg_score)"
                }
                if !satScores.sat_writing_avg_score.isEmpty {
                    self?.satWritingLabel.text = "Writing: \(satScores.sat_writing_avg_score)"
                }
            } else {
                self?.satScoreLabel.text = ""
            }
        }
    }
    
}

//
//  ViewController.swift
//  20190708-RussellStephenson-NYCSchools
//
//  Created by Russell Stephenson on 7/6/19.
//  Copyright © 2019 RussellApps. All rights reserved.
//

import UIKit

class SchoolListViewController: UIViewController {

    private let viewModel: SchoolListViewModel = SchoolListViewModel()
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.tableFooterView = UIView(frame: .zero)
        }
    }
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    /// MARK: View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        activityIndicator.startAnimating()
        viewModel.loadSchools { [weak self] in
            self?.activityIndicator.stopAnimating()
            self?.tableView.reloadData()
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is SchoolViewController,
            let viewController = segue.destination as? SchoolViewController,
            let indexPath = tableView.indexPathForSelectedRow,
            let selectedSchool = viewModel.getSchool(index: indexPath.row) {
            viewController.viewModel = SchoolViewModel(school: selectedSchool)
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }
    
    /// MARK: Actions
    @IBAction func sortTapped(_ sender: UIBarButtonItem) {
        let alertController = UIAlertController(title: nil, message: "Sort the schools by...", preferredStyle: .actionSheet)
        
        let sortByNameAscendingAction = UIAlertAction(title: "School Name from A to Z", style: .default) { [weak self] _ in
            self?.viewModel.sortSchools(sortOrder: .alphabeticalByName(ascending: true)) {
                self?.tableView.reloadData()
            }
        }
        let sortByNameDescendingAction = UIAlertAction(title: "School Name from Z to A", style: .default) { [weak self] _ in
            self?.viewModel.sortSchools(sortOrder: .alphabeticalByName(ascending: false)) {
                self?.tableView.reloadData()
            }
        }
        let sortByCityAscendingAction = UIAlertAction(title: "City from A to Z", style: .default) { [weak self] _ in
            self?.viewModel.sortSchools(sortOrder: .alphabeticalByCity(ascending: true)) {
                self?.tableView.reloadData()
            }
        }
        let sortByCityDescendingAction = UIAlertAction(title: "City from Z to A", style: .default) { [weak self] _ in
            self?.viewModel.sortSchools(sortOrder: .alphabeticalByCity(ascending: false)) {
                self?.tableView.reloadData()
            }
        }
        /// There's an error while sorting by graduation rate and I can't quite pin down what the issue is.
        let sortByGradRateAscendingAction = UIAlertAction(title: "Graduation Rate (Highest)", style: .default) { [weak self] _ in
            self?.viewModel.sortSchools(sortOrder: .numericallyByGraduationPercent(ascending: true)) {
                self?.tableView.reloadData()
            }
        }
        let sortByGradRateDescendingAction = UIAlertAction(title: "Graduation Rate (Lowest)", style: .default) { [weak self] _ in
            self?.viewModel.sortSchools(sortOrder: .numericallyByGraduationPercent(ascending: false)) {
                self?.tableView.reloadData()
            }
        }
        let sortByStudentsAscendingAction = UIAlertAction(title: "Number of Students (Highest)", style: .default) { [weak self] _ in
            self?.viewModel.sortSchools(sortOrder: .numericallyByNumberOfStudents(ascending: true)) {
                self?.tableView.reloadData()
            }
        }
        let sortByStudentsDescendingAction = UIAlertAction(title: "Number of Students (Lowest)", style: .default) { [weak self] _ in
            self?.viewModel.sortSchools(sortOrder: .numericallyByNumberOfStudents(ascending: false)) {
                self?.tableView.reloadData()
            }
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alertController.addAction(sortByNameAscendingAction)
        alertController.addAction(sortByNameDescendingAction)
        alertController.addAction(sortByCityAscendingAction)
        alertController.addAction(sortByCityDescendingAction)
        alertController.addAction(sortByGradRateAscendingAction)
        alertController.addAction(sortByGradRateDescendingAction)
        alertController.addAction(sortByStudentsAscendingAction)
        alertController.addAction(sortByStudentsDescendingAction)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
}

/// MARK: UITableView Delegate Methods
extension SchoolListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.schools?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: SchoolTableViewCell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.schoolCell, for: indexPath) as! SchoolTableViewCell
        if let school = viewModel.getSchool(index: indexPath.row) {
            cell.setCell(school: school)
        }
        
        return cell
    }
}

extension SchoolListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: Constants.SegueIdentifiers.schoolDetailSegue, sender: self)
    }
}


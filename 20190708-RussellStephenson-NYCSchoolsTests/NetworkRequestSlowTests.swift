//
//  NetworkRequestSlowTests.swift
//  20190708-RussellStephenson-NYCSchoolsTests
//
//  Created by Russell Stephenson on 7/7/19.
//  Copyright © 2019 RussellApps. All rights reserved.
//

import XCTest

class NetworkRequestSlowTests: XCTestCase {
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testCallToGetSchoolsRecieves200() {
        guard let url = URL(string: Constants.API.schoolListUrl) else {
            XCTFail("Bad URL")
            return
        }
        
        let promise = expectation(description: "Status code: 200")
        
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let error = error {
                XCTFail("Error: \(error.localizedDescription)")
                return
            } else if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                if statusCode == 200 {
                    promise.fulfill()
                } else {
                    XCTFail("Status code: \(statusCode)")
                }
            }
        }
        task.resume()
        
        wait(for: [promise], timeout: 5)
    }
    
    func testCallToGetSatReceives200() {
        guard let url = URL(string: Constants.API.satScoreUrl) else {
            XCTFail("Bad URL")
            return
        }
        
        let promise = expectation(description: "Status code: 200")
        
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let error = error {
                XCTFail("Error: \(error.localizedDescription)")
                return
            } else if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                if statusCode == 200 {
                    promise.fulfill()
                } else {
                    XCTFail("Status code: \(statusCode)")
                }
            }
        }
        task.resume()
        
        wait(for: [promise], timeout: 5)
    }
}

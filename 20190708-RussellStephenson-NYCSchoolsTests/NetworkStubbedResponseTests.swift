//
//  NetworkStubbedResponseTests.swift
//  20190708-RussellStephenson-NYCSchoolsTests
//
//  Created by Russell Stephenson on 7/7/19.
//  Copyright © 2019 RussellApps. All rights reserved.
//

import XCTest

class NetworkStubbedResponseTests: XCTestCase {
    
    override func setUp() {
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testSatStubbedResponse() {
        let testBundle = Bundle(for: type(of: self))
        let path = testBundle.path(forResource: "SatResponse", ofType: "json")
        let data = try? Data(contentsOf: URL(fileURLWithPath: path!), options: .alwaysMapped)
        
        guard let testData = data else {
            XCTFail("Bad data")
            return
        }
        
        let scoreList : [SatScore]?
        let decoder = JSONDecoder();
        decoder.dateDecodingStrategy = .iso8601
        do {
            scoreList = try decoder.decode(Array.self, from: testData)
        } catch {
            XCTFail("Failed to parse data")
            return
        }
        
        if let list = scoreList {
            XCTAssertEqual(list.count, 3, "Didn't parse 3 items from fake response")
        } else {
            XCTFail("Empty Score List")
        }
    }
    
    func testSchoolStubbedResponse() {
        let testBundle = Bundle(for: type(of: self))
        let path = testBundle.path(forResource: "SchoolListResponse", ofType: "json")
        let data = try? Data(contentsOf: URL(fileURLWithPath: path!), options: .alwaysMapped)
        
        guard let testData = data else {
            XCTFail("Bad data")
            return
        }
        
        let scoreList : [School]?
        let decoder = JSONDecoder();
        decoder.dateDecodingStrategy = .iso8601
        do {
            scoreList = try decoder.decode(Array.self, from: testData)
        } catch {
            XCTFail("Failed to parse data")
            return
        }
        
        if let list = scoreList {
            XCTAssertEqual(list.count, 3, "Didn't parse 3 items from fake response")
        } else {
            XCTFail("Empty School List")
        }
    }
}
